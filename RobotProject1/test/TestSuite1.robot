*** Settings ***
Library    SeleniumLibrary    

Suite Setup       Log    I'm inside test suite setup
Suite Teardown    Log    I'm inside test suite teardwon
Test Setup        Log    I'm inside test setup
Test Teardown     Log    I'm inside test teardown          

Default Tags    Sanity 

*** Test Cases ***

MyFirstTest
    [Tags]    smoke
    Log    Hello World

MySecondTest
    [Tags]    example
    Log    Hello World 2 
    # Set Tags    regression
    # Remove Tags    regression

MyThirdTest
    Log    Hello World 3

MyFourthTest
    Log    Hello World 4

FirstSeleniumTest
    Open Browser    https://google.com      chrome
    Set Browser Implicit Wait    5 
    Input Text      name=q                  Automation Step by Step
    Press Keys      name=q                  ENTER
    # Press Keys      name=q                  ESCAPE
    # Click Button    name=btnK    
    Sleep    2    
    Close Browser
    Log    Test Completed
    
SampleLoginTest
    [Documentation]    This is a sample login test
    Open Browser    ${URL}                chrome
    Set Browser Implicit Wait    5
    LoginKW
    Click Element   id=welcome
    Click Element   link=Logout
    Close Browser
    Log             Test Completed
    Log             This was executed by %{username} and on %{os}    
    
*** Variables ***
${URL}           https://opensource-demo.orangehrmlive.com/   
@{CREDENTIAL}    Admin    admin123
&{LOGINDATA}     username=Admin    password=admin123   

*** Keywords ***
LoginKW
    Input Text      name=txtUsername      ${CREDENTIAL}[0]
    Input Text      name=txtPassword      ${LOGINDATA}[password]
    Click Button    id=btnLogin